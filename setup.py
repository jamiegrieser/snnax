from distutils.core import setup
setup(name='snnax',
        version='0.1',
        description='A Jax-based package to simulate spiking neural networks.',
        author='Jan Finkbeiner, Jamie Grieser',
        packages=['snnax'],
        install_requires=[
            'numpy',
            'jax',
            'equinox'
        ] 
)

