from equinox import Module

class SNNModule(Module):

    def init_state(self, key):
        raise NotImplementedError

    def __call__(self, state, synaptic_input):
        raise NotImplementedError