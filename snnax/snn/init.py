from typing import Callable
import functools as ft
import jax
import jax.numpy as jnp
import jax.random as jrandom
import equinox as eqx

from jax.tree_util import tree_structure, tree_leaves, tree_map
from equinox.tree import tree_at
from jax.nn.initializers import zeros

from ..module import SNNModule
from ..utils.tree import apply_to_tree_leaf

def snn_init_fn(key, in_features, out_features, threshold, in_firing_rate):
    mean = .0 #1.0 #0.5
    thresh_dist_fac = 1. # TODO what to use ?
    postsyn_pot_kernal_variance = 0.5 # TODO what to use ?
    variance = (threshold / thresh_dist_fac)**2 / (in_features * in_firing_rate * postsyn_pot_kernal_variance)
    weights = jrandom.normal(key, (out_features, in_features))
    weights = jnp.sqrt(variance) * weights + mean
    return weights


def lsuv(model, batch, key: jrandom.PRNGKey, tgt_mean: float=-.85, tgt_var: float=1., mean_tol: float=.1, var_tol: float=.1):
    """
    LSUV = Layer Sequential Unit Variance
    Initialization inspired from Mishkin D and Matas J. All you need is a good init. arXiv:1511.06422 [cs],
    February 2016.
    This is a datadriven initialization scheme for possibly stateful networks which
    adjusts the mean and variance of the membrane output to the given values for each layer.
    The weights and biases are required to be pytree leaves with attributes `weight` and `bias`.
    It operates on an entire minibatch.
    """
    init_key, key = jrandom.split(key)
    shape = batch[0,0].shape
    init_states = model.init_state(shape, init_key)

    weight_init_fn = jax.nn.initializers.orthogonal()
    w_key, b_key = jrandom.split(key, 2)
    orthogonal_init_fn = lambda leaf: weight_init_fn(w_key, leaf.shape, leaf.dtype)
    zero_init_fn = lambda leaf: zeros(b_key, leaf.shape, leaf.dtype)

    # Initialize all layers with `weight` or `bias` attribute with 
    # random orthogonal matrices or zeros respectively 
    model = apply_to_tree_leaf(model, 'weight', orthogonal_init_fn)
    model = apply_to_tree_leaf(model, 'bias', zero_init_fn)

    adjust_var = lambda weight: weight*jnp.sqrt(tgt_var)/jnp.sqrt(jnp.maximum(mem_pot_var,1e-2))
    adjust_mean_bias = lambda bias: bias - .2*(mem_pot_mean - tgt_mean) 
    adjust_mean_weight = lambda eps, weight: weight * (1. - eps) # (1. - .2*(mem_pot_mean - tgt_mean)/norm) # TODO This needs further investigation!!!

    spike_layers = [i for i, layer in enumerate(model.layers) if isinstance(layer, SNNModule)]
    vmap_model = jax.vmap(model, in_axes=(None, 0))

    alldone = False
    while not alldone:
        alldone = True
        states, outs = vmap_model(init_states, batch)
        mem_pot_states = states[0] # index 0 contains neuron states

        spike_layer_idx = 0
        for ilayer, layer in enumerate(model.layers):
            # Filter for layers that have weight attribute
            if isinstance(layer, SNNModule):
                spike_layer_idx += 1
                continue

            idx = spike_layers[spike_layer_idx]

            spike_sum = outs[idx].sum(axis=1) # Sum of spikes over entire BPTT range
            mem_pot_var = jnp.var(mem_pot_states[idx][:,0,:]) # index 0 contains membrane potential
            mem_pot_mean = jnp.mean(mem_pot_states[idx][:,0,:]) # index 0 contains membrane potential
            spike_mean = jnp.mean(spike_sum)
            
            print(f"Layer: {idx}, Variance of U: {mem_pot_var:.3}, Mean of U: {mem_pot_mean:.3}, Mean of S: {spike_mean:.3}")
            if jnp.isnan(mem_pot_var) or jnp.isnan(mem_pot_mean):
                print('NaN encountered during init')
                done = False
                raise
        
            if jnp.abs(mem_pot_var - tgt_var) > var_tol:
                model.layers[ilayer] = apply_to_tree_leaf(layer, 'weight', adjust_var)
                done = False
            else:
                done = True
            alldone *= done
                
            if jnp.abs(mem_pot_mean - tgt_mean) > mean_tol:
                _has_bias = lambda leaf: hasattr(leaf, 'bias')
                # Initialization with or without bias
                bias_list = [getattr(leaf, 'bias') is not None for leaf in tree_leaves(layer, is_leaf=_has_bias) if _has_bias(leaf)]
                if all(bias_list):
                    model.layers[ilayer] = apply_to_tree_leaf(layer, 'bias', adjust_mean_bias)
                else:
                    eps = -.05 * jnp.sign(mem_pot_mean - tgt_mean)*jnp.abs(mem_pot_mean - tgt_mean)**2
                    adjust_weight = ft.partial(adjust_mean_weight, eps)
                    model.layers[ilayer] = apply_to_tree_leaf(layer, 'weight', adjust_weight)
                done = False
            else:
                done = True
            alldone *= done
            
    return model

