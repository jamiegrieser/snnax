from typing import Sequence, Union, Any, Callable, Optional

import jax
import jax.random as jrandom
import jax.numpy as jnp

from ...module import SNNModule

def zeros(shape_, key_):
    return jnp.zeros(shape_)

def ones(shape_, key_):
    return jnp.ones(shape_)

class BaseNeuron(SNNModule):
    """
    Base class to define custom spiking neuron types.
    """
    init_fn: Callable[[Sequence[int], Any], Any] 

    def __init__(self,init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None):
        if init_fn is None:
            init_fn = zeros
        self.init_fn = init_fn

    def init_state(self, shape: Union[Sequence[int], int], 
                        key: jrandom.PRNGKey, *args, **kwargs):
        return self.init_fn(shape, key, *args, **kwargs)

    def init_out(self, shape: Union[Sequence[int], int], key: jrandom.PRNGKey):
        # The initial ouput of the layer. Initialize as an array of zeros.
        return zeros(shape, key)

    def __call__(self, state, synaptic_input):
        raise NotImplementedError

