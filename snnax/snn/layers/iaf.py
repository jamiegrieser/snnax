from typing import Sequence, Union, Tuple, Any, Callable, Optional

import jax
import jax.random as jrandom
import jax.numpy as jnp

import equinox as eqx
from equinox.custom_types import Array

from .base import BaseNeuron, zeros, ones
from ...functional import softplus, inverse_softplus, get_spike_fn
from ...utils.util import apply_and_keep_pytype


class SimpleIAF(BaseNeuron):
    """
    Simple implementation of a layer of integrate-and-fire neurons 
    which does not make explicit use of synaptic currents.
    It integrates the raw synaptic input without any decay.
    Requires one constant to simulate constant membrane potential leak.
    """
    leak: Union[float, Array]
    threshold: Union[float, Array]
    spike_fn: Callable[[Array], Array]
    reset_val: Union[None, float, Array]
    stop_reset_grad: bool

    def __init__(self,
            spike_fn: Union[Callable, str],
            *args, 
            leak: Union[float, Array] = 0.,
            threshold: Union[float, Array] = 1.,
            reset_val: Optional[Union[float, Array]] = None,
            stop_reset_grad: Optional[bool] = True,
            init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None,
            **kwargs
        ) -> None:
        """**Arguments**:

        - `spike_fn`: Spike treshold function with custom surrogate gradient.
        - `leak`: Describes the constant leak of the membrane potential.
            Defaults to zero, i.e. no leak.
        - `threshold`: Spike threshold for membrane potential. Defaults to 1.
        - `reset_val`: Reset value after a spike has been emitted. Defaults to None.
        - `stop_reset_grad`: Boolean to control if the gradient is propagated
            through the refectory potential.
        - `init_fn`: Function to initialize the initial state of the spiking neurons.
            Defaults to initialization with zeros if nothing else is provided.
        """

        super().__init__(init_fn)
        
        assert jnp.all(leak >= 0.) # TODO test assertion
        # TODO assert for numerical stability 0.999 leads to errors...
        self.threshold = apply_and_keep_pytype(inverse_softplus, threshold)
        self.leak = apply_and_keep_pytype(inverse_softplus, leak)

        if isinstance(spike_fn, str):
            self.spike_fn = get_spike_fn(spike_fn, *args, **kwargs)
        else:
            self.spike_fn = spike_fn

        self.reset_val = apply_and_keep_pytype(inverse_softplus, reset_val) if reset_val is not None else reset_val
        self.stop_reset_grad = stop_reset_grad

    def __call__(self, membrane_potential: Array, synaptic_input: Array) -> Tuple[Any, Any]:
        leak = jax.nn.softplus(self.leak)
        threshold = jax.nn.softplus(self.threshold)

        membrane_potential = (membrane_potential - leak) + synaptic_input
        spikes_out = self.spike_fn(membrane_potential - threshold)
        
        if self.reset_val is None:
            reset_pot = membrane_potential*spikes_out
        else:
            reset_val = jax.nn.softplus(self.reset_val)
            reset_pot = reset_val * spikes_out
        # optionally stop gradient propagation through refectory potential       
        refectory_potential = jax.lax.stop_gradient(reset_pot) if self.stop_reset_grad else reset_pot
        membrane_potential = membrane_potential - refectory_potential

        output = spikes_out
        state = membrane_potential
        return state, output


# TODO Implement IAF neuron
class IAF(BaseNeuron):
    """
    Implementation of an integrate-and-fire neuron with a constant leak
    or no leak at all. However, it has no leak by default.
    """
    exp_decay_constants: Union[Sequence[float], Array]
    leak: Union[float, Array]
    threshold: Union[float, Array]
    spike_fn: Callable[[Array], Array]
    reset_val: Union[None, float, Array]
    stop_reset_grad: bool

    def __init__(self,
            decay_constants: Union[Sequence[float], Array],
            spike_fn: Callable,
            *args,
            leak: Union[float, Array] = 0.,
            threshold: Union[float, Array] = 1.,
            reset_val: Optional[Union[float, Array]] = None,
            stop_reset_grad: Optional[bool] = True,
            init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None,
            **kwargs
        ) -> None:
        """**Arguments**:

        - `decay_constants`: Decay constants for the leaky integrate-and-fire neuron.
            Index 0 describes the decay constant of the membrane potential,
            Index 1 describes the decay constant of the synaptic current.
        - `spike_fn`: Spike treshold function with custom surrogate gradient.
        - `leak`: Describes the constant leak of the membrane potential.
            Defaults to zero, i.e. no leak.
        - `threshold`: Spike threshold for membrane potential. Defaults to 1.
        - `reset_val`: Reset value after a spike has been emitted. Defaults to None.
        - `stop_reset_grad`: Boolean to control if the gradient is propagated
            through the refectory potential.
        - `init_fn`: Function to initialize the initial state of the spiking neurons.
            Defaults to initialization with zeros if nothing else is provided.
        """

        super().__init__(init_fn)
        
        assert jnp.all(leak >= 0.) # TODO test assertion
        # TODO assert for numerical stability 0.999 leads to errors...
        self.threshold = apply_and_keep_pytype(inverse_softplus, threshold)
        self.leak = apply_and_keep_pytype(inverse_softplus, leak)
        self.exp_decay_constants = decay_constants

        if isinstance(spike_fn, str):
            self.spike_fn = get_spike_fn(spike_fn, *args, **kwargs)
        else:
            self.spike_fn = spike_fn

        self.reset_val = apply_and_keep_pytype(inverse_softplus, reset_val) if reset_val is not None else reset_val
        self.stop_reset_grad = stop_reset_grad

    def init_state(self, shape: Union[Sequence[int], int], key: jrandom.PRNGKey, *args, **kwargs):
        init_state_mem_pot = self.init_fn(shape, key, *args, **kwargs)
        init_state_syn_curr = zeros(shape, key)
        # return (init_state_mem_pot, init_state_syn_curr)
        return jnp.concatenate([init_state_mem_pot, init_state_syn_curr])

    def __call__(self, state: Tuple[Array, Array], synaptic_input: Array) -> Tuple[Any, Any]:
        membrane_potential, syn_curr = state[0], state[1]
        leak = jax.nn.softplus(self.leak)
        alpha = jax.nn.sigmoid(self.exp_decay_constants[0])
        beta = jax.nn.sigmoid(self.exp_decay_constants[1])
        threshold = jax.nn.softplus(self.threshold)
        
        membrane_potential = (membrane_potential - leak) + (1. - alpha)*syn_curr
        syn_curr = beta*syn_curr + (1. - beta)*synaptic_input # Here the (1. - beta) seems to work fine
        spikes_out = self.spike_fn(membrane_potential - threshold)
        
        if self.reset_val is None:
            reset_pot = membrane_potential*spikes_out
        else:
            reset_val = jax.nn.softplus(self.reset_val)
            reset_pot = reset_val * spikes_out
        # optionally stop gradient propagation through refactory potential       
        refectory_potential = jax.lax.stop_gradient(reset_pot) if self.stop_reset_grad else reset_pot
        membrane_potential = membrane_potential - refectory_potential

        output = spikes_out
        state = jnp.concatenate([membrane_potential, syn_curr])
        return state, output

