import jax
import jax.numpy as jnp
import equinox as eqx


class Flatten(eqx.Module):
    """
    Simple module to flatten the output of a layer.
    """
    def __call__(self, x):
        return jnp.reshape(x, -1)

