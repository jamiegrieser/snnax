from typing import Sequence, Union, Tuple, Any, Callable, Optional

import jax
import jax.random as jrandom
import jax.numpy as jnp

import equinox as eqx
from equinox.custom_types import Array

from .base import BaseNeuron
from ...utils.util import apply_and_keep_pytype


class LI(BaseNeuron):
    """
    Implementation of a simple leaky integrator neuron layer which 
    integrates over the synaptic inputs.
    """
    # TODO link to which implementation we follow
    exp_decay_constants: Union[float, Array]

    def __init__(self,
            decay_constants: Union[float, Array],
            init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None,
        ) -> None:
        """**Arguments**:

        - `decay_constants`: Decay constant of the leaky integrator.
        - `init_fn`: Function to initialize the initial state of the spiking neurons.
            Defaults to initialization with zeros if nothing else is provided.
        """

        super().__init__(init_fn)
        assert decay_constants > 0. and decay_constants < 1.
        exp_decay_constants = apply_and_keep_pytype(lambda x: jnp.log(x / (1. - x)), decay_constants)
        self.exp_decay_constants = exp_decay_constants

    def __call__(self, membrane_potential: Array, synaptic_input: Array) -> Tuple[Any, Any]:
        alpha = jax.nn.sigmoid(self.exp_decay_constants)
        membrane_potential = alpha*membrane_potential + (1. - alpha)*synaptic_input
        
        output = membrane_potential
        state = membrane_potential
        return state, output

