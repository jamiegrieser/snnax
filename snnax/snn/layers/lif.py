from typing import Sequence, Union, Tuple, Any, Callable, Optional

import jax
import jax.random as jrandom
import jax.numpy as jnp
import numpy as np

import equinox as eqx
from equinox.custom_types import Array
from equinox.module import static_field

from .base import BaseNeuron, zeros, ones
from ...functional import softplus, inverse_softplus, get_spike_fn
from ...utils.util import apply_and_keep_pytype


class SimpleLIF(BaseNeuron):
    """
    Simple implementation of a layer of leaky integrate-and-fire neurons 
    which does not make explicit use of synaptic currents.
    Requires one decay constant to simulate membrane potential leak.
    """
    exp_decay_constants: Union[Sequence[float], Array]
    threshold: Union[float, Array]
    spike_fn: Callable[[Array], Array]
    reset_val: Union[None, float, Array]
    stop_reset_grad: bool = static_field()

    def __init__(self,
            decay_constants: Union[Sequence[float], Array],
            spike_fn: Union[Callable, str],
            *args, 
            threshold: Union[float, Array] = 1.0,
            reset_val: Optional[Union[float, Array]] = None,
            stop_reset_grad: Optional[bool] = True,
            init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None,
            **kwargs
        ) -> None:
        """**Arguments**:

        - `decay_constant`: Decay constant of the integrate-and-fire neuron.
        - `spike_fn`: Spike treshold function with custom surrogate gradient.
        - `threshold`: Spike threshold for membrane potential. Defaults to 1.
        - `reset_val`: Reset value after a spike has been emitted. Defaults to None.
        - `stop_reset_grad`: Boolean to control if the gradient is propagated
            through the refectory potential.
        - `init_fn`: Function to initialize the initial state of the spiking neurons.
            Defaults to initialization with zeros if nothing else is provided.
        """

        super().__init__(init_fn)

        # if isinstance(decay_constants, Sequence):
        #     decay_constants = jnp.array(decay_constants)
        
        # assert jnp.all(decay_constants > 0.) and jnp.all(decay_constants < 1.) # TODO test assertion

        # TODO assert for numerical stability 0.999 leads to errors...
        self.threshold = apply_and_keep_pytype(inverse_softplus, threshold)
        print("asdf", type(decay_constants))
        self.exp_decay_constants = [apply_and_keep_pytype(lambda x: jnp.log(x / (1. - x)), decay_constants[0])]
        print(type(self.exp_decay_constants))

        if isinstance(spike_fn, str):
            self.spike_fn = get_spike_fn(spike_fn, *args, **kwargs)
        else:
            self.spike_fn = spike_fn

        self.reset_val = apply_and_keep_pytype(inverse_softplus, reset_val) if reset_val is not None else reset_val
        self.stop_reset_grad = stop_reset_grad

    def __call__(self, membrane_potential: Array, synaptic_input: Array) -> Tuple:
        alpha = jax.nn.sigmoid(self.exp_decay_constants[0])
        threshold = jax.nn.softplus(self.threshold)
        
        membrane_potential = alpha*membrane_potential + (1. - alpha)*synaptic_input # TODO with (1-alpha) or without ?
        spikes_out = self.spike_fn(membrane_potential - threshold)
        
        if self.reset_val is None:
            reset_pot = membrane_potential*spikes_out
        else:
            reset_val = jax.nn.softplus(self.reset_val)
            reset_pot = reset_val * spikes_out
        # optionally stop gradient propagation through refectory potential       
        refectory_potential = jax.lax.stop_gradient(reset_pot) if self.stop_reset_grad else reset_pot
        membrane_potential = membrane_potential - refectory_potential

        output = spikes_out
        state = membrane_potential
        return state, output

class SimpleRecurrentLIF(SimpleLIF):
    """
    Simple implementation of a recurrent leaky 
    integrate-and-fire neuron.
    """
    # weight: jnp.ndarray

    def __init__(self,
            decay_constants: Union[Sequence[float], Array],
            spike_fn: Union[Callable, str],
            *args, 
            threshold: Union[float, Array] = 1.0,
            reset_val: Optional[Union[float, Array]] = None,
            stop_reset_grad: Optional[bool] = True,
            init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None,
            **kwargs
        ) -> None:
        """**Arguments**:

        - `decay_constant`: Decay constant of the integrate-and-fire neuron.
        - `spike_fn`: Spike treshold function with custom surrogate gradient.
        - `threshold`: Spike threshold for membrane potential. Defaults to 1.
        - `reset_val`: Reset value after a spike has been emitted. Defaults to None.
        - `stop_reset_grad`: Boolean to control if the gradient is propagated
            through the refectory potential.
        - `init_fn`: Function to initialize the initial state of the spiking neurons.
            Defaults to initialization with zeros if nothing else is provided.
        """
        super().__init__(decay_constants,
                        spike_fn,
                        *args, 
                        threshold=threshold,
                        reset_val=reset_val,
                        stop_reset_grad=stop_reset_grad,
                        init_fn=init_fn,
                        **kwargs)

        wkey = jrandom.PRNGKey(1243)
        # self.weight = jrandom.uniform(
        #     wkey, (16, 16), minval=-np.sqrt(1. / 16), maxval=np.sqrt(1. / 16)
        # )

    def init_state(self, shape: Union[Sequence[int], int], key: jrandom.PRNGKey, *args, **kwargs):
        wkey, init_key = jrandom.split(key, 2)

        hidden_size = shape[0]
        lim = np.sqrt(1. / hidden_size)

        w = jrandom.uniform(
            wkey, (hidden_size, hidden_size), minval=-lim, maxval=lim
        )
        # object.__setattr__(self, 'weight', w) # Dangerous hack!

        return super().init_state(shape, init_key, *args, **kwargs)


    def __call__(self, 
                membrane_potential: Array, 
                synaptic_input: Array
                ) -> Tuple:

        # l = self.weight.shape[0]
        external_input = synaptic_input[:16] # , recurrent_input = synaptic_input[:l], synaptic_input[l:]
        alpha = jax.nn.sigmoid(self.exp_decay_constants[0])
        threshold = jax.nn.softplus(self.threshold)
        
        membrane_potential = alpha*membrane_potential + (1. - alpha)*(external_input) # TODO with (1-alpha) or without ?
        spikes_out = self.spike_fn(membrane_potential - threshold)
        
        if self.reset_val is None:
            reset_pot = membrane_potential*spikes_out
        else:
            reset_val = jax.nn.softplus(self.reset_val)
            reset_pot = reset_val * spikes_out
        # optionally stop gradient propagation through refectory potential       
        refectory_potential = jax.lax.stop_gradient(reset_pot) if self.stop_reset_grad else reset_pot
        membrane_potential = membrane_potential - refectory_potential

        output = spikes_out
        state = membrane_potential
        return state, output


class LIF(BaseNeuron):
    """
    Implementation of a leaky integrate-and-fire neuron with
    synaptic currents. Requires two decay constants to describe
    decay of membrane potential and synaptic current.
    """
    exp_decay_constants: Union[Sequence[float], Array]
    threshold: Union[float, Array]
    spike_fn: Callable[[Array], Array]
    reset_val: Union[None, float, Array]
    stop_reset_grad: bool

    def __init__(self, 
            decay_constants: Union[Sequence[float], Array],
            spike_fn: Union[Callable, str],
            *args,
            threshold: Union[float, Array] = 1.,
            reset_val: Optional[Union[float, Array]] = None,
            stop_reset_grad: Optional[bool] = True,
            init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None,
            **kwargs
        ) -> None:
        """**Arguments**:

        - `shape`: Shape of the neuron layer.
        - `decay_constants`: Decay constants for the leaky integrate-and-fire neuron.
            Index 0 describes the decay constant of the membrane potential,
            Index 1 describes the decay constant of the synaptic current.
        - `spike_fn`: Spike treshold function with custom surrogate gradient.
        - `threshold`: Spike threshold for membrane potential. Defaults to 1.
        - `reset_val`: Reset value after a spike has been emitted. Defaults to None.
        - `stop_reset_grad`: Boolean to control if the gradient is propagated
            through the refectory potential.
        - `init_fn`: Function to initialize the initial state of the spiking neurons.
            Defaults to initialization with zeros if nothing else is provided.
        """

        super().__init__(init_fn)
        
        if isinstance(decay_constants, Sequence):
            decay_constants = jnp.array(decay_constants)

        assert jnp.all(decay_constants > 0.) and jnp.all(decay_constants < 1.) # TODO test assertion
        # TODO assert for numerical stability 0.999 leads to errors...
        self.threshold = apply_and_keep_pytype(inverse_softplus, threshold)
        self.exp_decay_constants = decay_constants # apply_and_keep_pytype(lambda x: jnp.log(x / (1. - x)), decay_constants)
        
        if isinstance(spike_fn, str):
            self.spike_fn = get_spike_fn(spike_fn, *args, **kwargs)
        else:
            self.spike_fn = spike_fn

        self.reset_val = apply_and_keep_pytype(inverse_softplus, reset_val) if reset_val is not None else reset_val
        self.stop_reset_grad = stop_reset_grad

    def init_state(self, shape, key, *args, **kwargs):
        init_state_mem_pot = self.init_fn(shape, key, *args, **kwargs)
        init_state_syn_curr = zeros(shape, key) # The synaptic currents are initialized as zeros
        return jnp.stack([init_state_mem_pot, init_state_syn_curr])

    def __call__(self, state: Tuple[Array, Array], synaptic_input: Array) -> Tuple[Any, Any]:
        membrane_potential, syn_curr = state[0], state[1]

        alpha = jax.nn.sigmoid(self.exp_decay_constants[0])
        beta = jax.nn.sigmoid(self.exp_decay_constants[1])
        threshold = softplus(self.threshold)
        
        membrane_potential = alpha*membrane_potential + (1. - alpha)*syn_curr
        syn_curr = beta*syn_curr + synaptic_input # TODO with (1. - beta) or not --> seems to work better without

        spike_output = self.spike_fn(membrane_potential - threshold)
        
        if self.reset_val is None:
            reset_pot = membrane_potential * spike_output
        else:
            reset_val = jax.nn.softplus(self.reset_val)
            reset_pot = reset_val * spike_output
        # optionally stop gradient propagation through refectory potential       
        refectory_potential = jax.lax.stop_gradient(reset_pot) if self.stop_reset_grad else reset_pot
        membrane_potential = membrane_potential - refectory_potential

        state = jnp.stack([membrane_potential, syn_curr]) # jnp.concatenate([membrane_potential, syn_curr])
        return state, spike_output

# TODO Adjust to new API
class AdaptiveExponentialLIF(BaseNeuron):
    """
    Implementation of a adaptive exponential leaky integrate-and-fire neuron
    as presented in  https://neuronaldynamics.epfl.ch/online/Ch6.S1.html.

    """
    exp_decay_constants: Union[float, Array]
    threshold: Union[float, Array]
    spike_fn: Callable[[Array], Array]
    reset_val: Union[float, Array]
    ada_step_val: Union[float, Array]
    ada_decay_constant: Union[float, Array]
    ada_coupling_var: Union[float, Array]
    stop_reset_grad: bool

    def __init__(self,
            decay_constants: Union[float, Array],
            threshold: Union[float, Array],
            spike_fn: Callable,
            ada_decay_constant: Union[float, Array],
            ada_step_val: Union[float, Array],
            ada_coupling_var: Union[float, Array],
            *args,
            reset_val: Optional[Union[float, Array]] = None,
            stop_reset_grad: Optional[bool] = True,
            init_fn: Optional[Callable[[Sequence[int], Any], Any]] = None,
            **kwargs
        ) -> None:
        """**Arguments**:

        - `decay_constants`: Decay constants for the adaptive leaky integrate-and-fire neuron.
            Index 0 describes the decay constant of the membrane potential,
            Index 1 describes the decay constant of the synaptic current.
        - `spike_fn`: Spike treshold function with custom surrogate gradient.
        - `threshold`: Spike threshold for membrane potential. Defaults to 1.
        - `reset_val`: Reset value after a spike has been emitted. Defaults to None.
        - `stop_reset_grad`: Boolean to control if the gradient is propagated
            through the refectory potential.
        - `init_fn`: Function to initialize the initial state of the spiking neurons.
            Defaults to initialization with zeros if nothing else is provided.
        """

        super().__init__(init_fn)
        
        assert jnp.all(decay_constants > 0.) and jnp.all(decay_constants < 1.) # TODO test assertion
        # TODO assert for numerical stability 0.999 leads to errors...
        self.threshold = apply_and_keep_pytype(inverse_softplus, threshold)
        self.exp_decay_constants = apply_and_keep_pytype(lambda x: jnp.log(x / (1. - x)), decay_constants)

        if isinstance(spike_fn, str):
            self.spike_fn = get_spike_fn(spike_fn, *args, **kwargs)
        else:
            self.spike_fn = spike_fn

        self.reset_val = apply_and_keep_pytype(inverse_softplus, reset_val) if reset_val is not None else reset_val
        self.stop_reset_grad = stop_reset_grad
        self.ada_decay_constant = apply_and_keep_pytype(lambda x: jnp.log(x/(1-x)), ada_decay_constant)
        self.ada_step_val = apply_and_keep_pytype(inverse_softplus, ada_step_val)
        self.ada_coupling_var = apply_and_keep_pytype(inverse_softplus, ada_coupling_var)

    def init_state(self, shape: Union[Sequence[int], int], key: jrandom.PRNGKey, *args, **kwargs):
        init_state_mem_pot = self.init_fn(shape, key, *args, **kwargs)
        init_state_ada = zeros(shape, key)
        init_state = jnp.concatenate([init_state_mem_pot, init_state_ada])
        return init_state

    def __call__(self, state: Array, synaptic_input: Array) -> Tuple[Any, Any]:

        membrane_potential, ada_var = state[:int(self.shape[0])], state[int(self.shape[0]):]

        alpha = jax.nn.sigmoid(self.exp_decay_constants)
        threshold = jax.nn.softplus(self.threshold)
        ada_decay_constant = jax.nn.sigmoid(self.ada_decay_constant)
        ada_step_val = jax.nn.softplus(self.ada_step_val)
        ada_coupling_var = jax.nn.softplus(self.ada_coupling_var)

        membrane_potential_new = alpha*membrane_potential + (synaptic_input + ada_var)
        # membrane_potential_new = alpha*membrane_potential + (1-alpha)*(synaptic_input + ada_var) # TODO with (1-alpha) or without ?
        spikes_out = self.spike_fn(membrane_potential_new - threshold)
        ada_var_new = (1. - ada_decay_constant)*ada_coupling_var*(membrane_potential_new - threshold) + ada_decay_constant*ada_var - ada_step_val*jax.lax.stop_gradient(spikes_out)

        if self.reset_val is None:
            reset_pot = membrane_potential_new*spikes_out
        else:
            reset_val = jax.nn.softplus(self.reset_val)
            reset_pot = reset_val * spikes_out
        # optionally stop gradient propagation through refectory potential       
        refectory_potential = jax.lax.stop_gradient(reset_pot) if self.stop_reset_grad else reset_pot
        membrane_potential_new = membrane_potential_new - refectory_potential

        output = spikes_out
        # state = (membrane_potential_new, ada_var_new)
        state = jnp.concatenate([membrane_potential_new, ada_var_new])
        return state, output

