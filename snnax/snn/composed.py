from typing import Sequence, Tuple, Optional, Any
import jax
import jax.numpy as jnp
import jax.random as jrandom

import equinox as eqx

from .architecture import SNNModel, SNNGraphStructure
from .utils import gen_output_connectivity


class Sequential(SNNModel):
    """
    Convenience class to construct a feed-forward spiking neural network in a
    simple manner. It supports the defined base neuron types as well as equinox layers. 
    Under the hood it constructs a connectivity graph with a feed-forward structure and 
    feeds it to the SNNModel class.

    TODO add API documentation and convenience methods to get length, topology 
    as well as a simple way to add recurrent connections to the base blocks?
    """

    def __init__(self, 
                layers: Sequence[eqx.Module], 
                ) -> None:
        """**Arguments**:

        - `layers`: Sequence containing the layers of the network in causal
            order.
        """
        num_layers = len(layers)
        input_connectivity, input_layer_ids, final_layer_ids = gen_feed_forward_struct(num_layers)

        # Constructing the connectivity graph
        graph_structure = SNNGraphStructure(
            num_layers = num_layers,
            input_layer_ids = input_layer_ids,
            final_layer_ids = final_layer_ids,
            input_connectivity = input_connectivity,
        )

        super().__init__(graph_structure, layers)

    def __getitem__(self, idx) -> Any:
        return self.layers[idx]

    def __len__(self) -> int:
        return len(self.layers)

    def __call__(self, state, batch) -> Tuple[Any, Any]:
        return super().__call__(state, batch)

    # TODO Implement __init__ and __call__ such that user is oblivious of output management and only has 
    # to pass the initial state as well as the external input data for every call.


def gen_feed_forward_struct(num_layers: int) -> Tuple[Sequence[int], Sequence[int], Sequence[int]]:
    """
    Function to construct a simple feed-forward connectivity graph from the
    given number of layers. This means that every layer is just connected to 
    the next one. 
    """
    input_connectivity = [[id] for id in range(-1, num_layers-1)]
    input_connectivity[0] = []
    input_layer_ids = [0]
    final_layer_ids = [num_layers-1]
    return input_connectivity, input_layer_ids, final_layer_ids

