from typing import NamedTuple, Sequence, Tuple

import jax
import jax.random as jrandom
import jax.numpy as jnp
import equinox as eqx
from equinox.module import static_field

from ..module import SNNModule

class SNNGraphStructure(NamedTuple):
    """
    This class contains meta-information about the computational graph.
    It can be used in conjunction with the SNNModel class to construct 
    a computational model.

    **Arguments**:

        - `num_layers`: Specifies the number of layers we want to have in our model.
        - `input_layer_ids`: Specifies which layers are provided with external input
        - `final_layer_ids`: Specifies which layers provide the output of the model.
        - `input_connectivity`: Specifies how the layers are connected to each other. 
        - `ouput_connectivity`: TODO TBD if we really need this!
    """
    num_layers: int
    input_layer_ids: Sequence[int]
    final_layer_ids: Sequence[int]
    input_connectivity: Sequence[Sequence[int]]
    

class SNNModel(SNNModule):
    """
    Class that allows the creation of custom SNNs with almost arbitrary 
    connectivity defined through a graph structure.
    """
    graph_structure: SNNGraphStructure = static_field()
    layers: Sequence[eqx.Module]

    def __init__(self, graph_structure: SNNGraphStructure, 
                        layers: Sequence[SNNModule],
                        ) -> None:
        """**Arguments**:

        - `graph_structure`: SNNGraphStructure object to specify network topology.
        - `layers`: Computational building blocks of the model.
        """
        super().__init__()

        self.graph_structure = graph_structure
        self.layers = layers

        assert len(layers) == self.graph_structure.num_layers
        assert len(self.graph_structure.input_connectivity) == self.graph_structure.num_layers


    def init_state(self, in_shape, key) -> Tuple:
        # TODO write proper state init!!!
        # This init works well for sequential architectures,
        # generalize it!
        keys = jrandom.split(key, len(self.layers))
        states, outs = [], []
        for key, layer in zip(keys, self.layers):
            if isinstance(layer, SNNModule):
                state = layer.init_state(in_shape, key)
                out = layer.init_out(in_shape, key)
                in_shape = out.shape
                states.append(state)
                outs.append(out)
            # This allows the usage of modules from equinox
            # by calculating the output shape with a mock input
            elif isinstance(layer, eqx.Module):
                mock_input = jnp.zeros(in_shape)
                out = layer(mock_input)
                in_shape = out.shape
                states.append(None)
                outs.append(out)
            else:
                raise f"Layer of type {type(layer)} not supported!"
        return states, outs


    def __call__(self, state, batch) -> Tuple:
        # TODO This call function needs optimization,
        # I am positive that this will be a computational bottleneck
        # in the future.
        
        def forward_fn(carry, input_batch) -> Tuple:
            states_new = []
            outs_new = []
            
            snn_states, outs = carry

            for inode, (snn_state, snn_layer) in enumerate(zip(snn_states, self.layers)):
                # Grab output from nodes for which the connectivity graph specifies a connection
                input_from_nodes = [outs[input_idx] for input_idx in self.graph_structure.input_connectivity[inode]]

                # If the node is also an input layer, also append external input
                if inode in self.graph_structure.input_layer_ids:
                    input_from_nodes.append(input_batch)
                inputs = jnp.concatenate(input_from_nodes, axis=-1)

                # Check if node is a SNNModule, i.e. a stateful layer
                if isinstance(snn_layer, SNNModule):
                    stat, out  = snn_layer(snn_state, inputs)
                    states_new.append(stat)
                    outs_new.append(out)
                else:
                    # for stateless layers
                    out = snn_layer(inputs)
                    states_new.append(None)
                    outs_new.append(out)

            carry_new = (states_new, outs_new)
            return carry_new, outs_new

        states, outs = jax.lax.scan(forward_fn, state, batch) # Performes actual BPTT
        # TODO optimize use of list comprehensions etc.
        # Do we even need this?
        # final_states = ([states[0][fin_id] for fin_id in snn_structure.final_layer_ids], 
        #                    [states[0][fin_id] for fin_id in snn_structure.final_layer_ids])
        # final_outs = [outs[fin_id] for fin_id in snn_structure.final_layer_ids]
        return states, outs     

