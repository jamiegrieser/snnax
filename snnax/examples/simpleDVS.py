import os
import functools as ft

import jax
import jax.numpy as jnp
import jax.nn as nn
import jax.random as jrand
from jax.tree_util import tree_map

import optax
import snnax as snx
import snnax.snn as snn
from snnax.utils.data import DataLoader
import equinox as eqx

import numpy as np
from tqdm import tqdm

import tonic
import tonic.transforms as transforms

os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

N_CLASSES = 11 # Number of classes
N_EPOCHS = 100 # Number of training epochs
T = 150 # Number of timesteps per epoch
INPUT_SHAPE = (2, 32, 32)
BATCH_SIZE = 16

SEED = 42 
key = jrand.PRNGKey(SEED)
init_key, key = jrand.split(key, 3)

transform = transforms.Compose([transforms.Denoise(filter_time=10000),
                                transforms.Downsample(time_factor=1, spatial_factor=0.25),
                                transforms.ToFrame(sensor_size=(32, 32, 2), n_time_bins=T),
                                transforms.NumpyAsType(np.float32)
                                ])

train_dataset = tonic.datasets.DVSGesture(save_to='./data', train=True, transform=transform)
train_dataloader = DataLoader(train_dataset,
                                batch_size=BATCH_SIZE, 
                                num_workers=8,
                                drop_last=True)

test_dataset = tonic.datasets.DVSGesture(save_to='./data', train=False, transform=transform)
test_dataloader = DataLoader(test_dataset, batch_size=1, num_workers=4)


def one_hot_cross_entropy(prediction, target_one_hot):
    """
    Calculate one-hot cross-entropy for a single prediction and target.
    This will be vectorized using vmap.
    """
    return -jnp.sum(target_one_hot*jax.nn.log_softmax(prediction)) / len(target_one_hot)

def calc_loss(model, init_state, data, target, loss_fn):
    """
    Here we define how the loss is exacly calculated, i.e. whether
    we use a sum of spikes or spike-timing for the calculation of
    the cross-entropy.
    """
    states, outs = model(init_state, data)
    final_layer_out = outs[-1] # TODO adjust to new API
    pred = tree_map(lambda x: jnp.sum(x, axis=0), final_layer_out) # sum over all spikes

    loss_val = loss_fn(pred, target)
    return loss_val

# Vectorization of the loss function calculation
vmap_calc_loss = jax.vmap(calc_loss, in_axes=(None, None, 0, 0, None))

def calc_batch_loss(model, init_state, input_batch, target, loss_fn):
    """
    The vectorized version of calc_loss is used to 
    to get the batch loss.
    """
    loss_vals = vmap_calc_loss(model, init_state, input_batch, target, loss_fn)
    loss_val = loss_vals.sum() # summing over all losses of the batch
    return loss_val

def calc_loss_and_grads(model, init_state, input_batch, target, loss_fn, key):
    """
    This function uses the filter_value_and_grad feature of equinox to calculate the
    value of the batch loss as well as it's gradients w.r.t. the models parameters.
    """
    loss_val, grad = eqx.filter_value_and_grad(calc_batch_loss)(model, init_state, input_batch, target, loss_fn)
    return loss_val, grad

def update(calc_loss_and_grads, optim, loss_fn, model, opt_state, input_batch, target, key):
    """
    Function to calculate the update of the model and the optimizer based
    on the calculated updates.
    """
    init_key, grad_key = jax.random.split(key)
    model_states = model.init_state(INPUT_SHAPE, init_key)
    loss_value, grads = calc_loss_and_grads(model, model_states, input_batch, target, loss_fn, grad_key)    

    updates, opt_state = optim.update(grads, opt_state)
    model = eqx.apply_updates(model, updates)
    return model, opt_state, loss_value

### Functions for calculating the accuracy
@ft.partial(eqx.filter_jit, filter_spec=eqx.is_array)
@ft.partial(jax.vmap, in_axes=(None, None, 0))
def predict(model, state, data):
    return model(state, data)

@ft.partial(eqx.filter_jit, filter_spec=eqx.is_array)
def calc_accuracy(model, state, data, target, normalized=True):
    _, out = predict(model, state, data)
    pred = tree_map(lambda x: jnp.sum(x, axis=1), out[-1]).argmax(axis=-1) # sum over spikes

    if normalized:
        return (pred == target).mean()
    else:
        return (pred == target).sum()

key1, key2, key3, key4, key = jrand.split(key, 5)

model = snn.Sequential([
    eqx.nn.Conv2d(2, 16, 7, key=key1, use_bias=False),
    snn.LIF([.95, .85], "superspike"),

    eqx.nn.Conv2d(16, 16, 7, key=key2, use_bias=False),
    snn.LIF([.95, .85], "superspike"),

    eqx.nn.Conv2d(16, 16, 7, key=key3, use_bias=False),
    snn.LIF([.95, .85], "superspike"),

    snn.Flatten(),
    eqx.nn.Linear(3136, 11, key=key4, use_bias=False),
    snn.LIF([.95, .85], "superspike")
])

init_batch = jnp.asarray(next(iter(train_dataloader))[0], dtype=jnp.float32)
model = snn.init.lsuv(model, init_batch, init_key, var_tol=0.6, mean_tol=0.6)

loss_vals, accuracies, accuracies_train = [], [], []
    
optim = optax.adam(1e-2)
opt_state = optim.init(eqx.filter(model, eqx.is_inexact_array))

loss_fn = one_hot_cross_entropy
update_method = ft.partial(update, calc_loss_and_grads, optim, loss_fn)

nbar = tqdm(range(N_EPOCHS))
for epoch in nbar:
    loss_vals, accuracies, accuracies_train = [], [], []
    pbar = tqdm(train_dataloader, leave=False)
    batch_key, key = jax.random.split(key)
    for input_batch, target_batch in pbar:
        input_batch = jnp.asarray(input_batch, dtype=jnp.float32)
        hot_target_batch = jnp.asarray(nn.one_hot(target_batch, N_CLASSES), dtype=jnp.float32)

        model, opt_state, loss_value = eqx.filter_jit(update_method)(model, opt_state, input_batch, hot_target_batch, tree_map(jnp.asarray, batch_key))
        loss_vals.append(loss_value/BATCH_SIZE)
        
        pbar.set_description(f"Loss: {loss_value/BATCH_SIZE}")
        acc_train = calc_accuracy(model, model.init_state(INPUT_SHAPE, batch_key), 
                                jnp.asarray(input_batch, dtype=jnp.float32), 
                                jnp.asarray(target_batch, dtype=jnp.float32))

        accuracies_train.append(acc_train)

    tbar = tqdm(test_dataloader, leave=False)
    batch_key, key = jax.random.split(key)
    
    for input_test, target_test in tbar:
        acc_val = calc_accuracy(model, model.init_state(INPUT_SHAPE, batch_key), 
                                jnp.asarray(input_test, dtype=jnp.float32), 
                                jnp.asarray(target_test, dtype=jnp.float32))
        accuracies.append(acc_val)

    nbar.set_description(f"epoch: {epoch}, loss = {np.mean(loss_vals)}, accuracy_train = {np.mean(accuracies_train)}, accuracy_val = {np.mean(accuracies)}")

