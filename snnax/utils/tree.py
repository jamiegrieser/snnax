from typing import Callable

import jax
import jax.numpy as jnp
from jax.tree_util import tree_leaves

import equinox as eqx
from equinox.tree import tree_at

def apply_to_tree_leaf(pytree, identifier: str, replace_fn: Callable):
    """
    Apply a function all leaves in the given pytree.
    To simply replace values, use `replace_fn=lambda _: value`
    
    **Arguments**
    - `pytree`: The pytree where we want to edit the leaves.
    - `identifier`: A string used to identify the name/field of the leaves.
    - `replace_fn`: Callable which is applied to the leaf values.
    """
    _has_identifier = lambda leaf: hasattr(leaf, identifier) and getattr(leaf, identifier) is not None
    def _identifier(pytree):
        return tuple(
            getattr(leaf, identifier)
            for leaf in tree_leaves(pytree, is_leaf=_has_identifier)
            if _has_identifier(leaf)
        )

    return tree_at(_identifier, pytree, replace_fn=replace_fn)

