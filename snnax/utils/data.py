import numpy as np
import jax
import jax.numpy as jnp

from.dataloaders import AbstractDataLoader, SimpleDataLoader, MultiprocessingDataLoader
from typing import Union, Iterable, Tuple, Callable, Optional, Any


# taken from https://stackoverflow.com/questions/19349410/how-to-pad-with-zeros-a-tensor-along-some-axis-python
def pad_along_axis(array, target_length: int, axis: int = 0) -> np.ndarray:
    pad_size = target_length - array.shape[axis]

    if pad_size <= 0:
        return array

    npad = [(0, 0)] * array.ndim
    npad[axis] = (0, pad_size)

    return np.pad(array, pad_width=npad, mode='constant', constant_values=0)

def check_identical_elements(elements) -> bool:
    element0 = elements[0]
    for element in elements[1:]:
        if element != element0:
            return False
    return True

def pad_and_stack(batch) -> np.ndarray:
    seq_lens = [len(arr) for arr in batch]
    if check_identical_elements(seq_lens):
        return np.stack(batch)
    max_len = max(seq_lens)
    stacked_tensor = np.empty((len(batch), max_len, *batch[0].shape[1:]), dtype=batch[0].dtype)
    for i, (seq_len, arr) in enumerate(zip(seq_lens, batch)):
        stacked_tensor[i, :seq_len] = arr
        stacked_tensor[i, seq_len:] = 0
    return np.asarray(stacked_tensor)


# mainly taken from jax demo: https://jax.readthedocs.io/en/latest/notebooks/Neural_Network_and_Data_Loading.html
def sequence_collate(batch) -> np.ndarray:
    if isinstance(batch[0], (np.ndarray, jnp.ndarray)):
        return pad_and_stack(batch)
        
    elif isinstance(batch[0], (tuple,list)):
        transposed = zip(*batch)
        return [sequence_collate(samples) for samples in transposed]
    else:
        return np.array(batch)


# taken from jax demo: https://jax.readthedocs.io/en/latest/notebooks/Neural_Network_and_Data_Loading.html
def _collate(batch):
    if isinstance(batch[0], np.ndarray):
        return np.stack(batch)
    elif isinstance(batch[0], jnp.ndarray):
        return jnp.stack(batch)    
    elif isinstance(batch[0], (tuple, list)):
        transposed = zip(*batch)
        return [_collate(samples) for samples in transposed]
    else:
        raise NotImplementedError

# taken from jax demo: https://jax.readthedocs.io/en/latest/notebooks/Neural_Network_and_Data_Loading.html
class FlattenAndCast(object):
    def __call__(self, events, sensor_size, ordering, images=None, multi_image=None):
        # return np.ravel(np.array(events, dtype=np.float32)), images, sensor_size
        return np.reshape(np.asarray(events, dtype=np.float32), (events.shape[0], -1)), images, sensor_size       


class Dataset(object):
    """
    Generic dataset class.
    """
    def __getitem__(self):
        raise NotImplementedError

    def __len__(self):
        raise NotImplementedError    


def DataLoader(dataset: Any, *, num_workers: int = 0, **kwargs):
    """
    TODO documentation for wrapper function
    """
    if num_workers == 0:
        return SimpleDataLoader(dataset, **kwargs)
    elif num_workers > 0:
        return MultiprocessingDataLoader(dataset, num_workers=num_workers, **kwargs)
    else:
        raise ValueError(f"{num_workers} is not a valid number of workers!")

