from typing import Callable

def apply_and_keep_pytype(func, x):
    type_x = type(x)
    y = func(x)
    if isinstance(type_x, Callable):
        y = type_x(y)
    return y

