import numpy as np
import jax.numpy as jnp
from equinox.custom_types import Array
from typing import Optional


def convert_spike_raster_to_times(spike_train: Array, neuron_dim: int = -1, timestep: float = 1.0):
    assert len(spike_train.shape) < 3, "For now, only single spike train of multiple neurons "\
                                        "or multiple spike trains of a single neuron are supported, "\
                                        "but not multiple spike trains of multiple neurons."

    single_mode = True if len(spike_train.shape) == 1 else False
    num_neurons = spike_train.shape[neuron_dim]

    if single_mode:
        spike_times = _single_spike_train_to_times(spike_train, timestep)
    else:
        if neuron_dim != 0:
            spike_train = jnp.moveaxis(spike_train, neuron_dim,0)

        spike_times = []
        for neuron,neuron_spikes in zip(range(num_neurons), spike_train):
            neuron_spike_times = _single_spike_train_to_times(neuron_spikes, timestep)
            spike_times.append(neuron_spike_times)
    
    return spike_times

def _single_spike_train_to_times(neuron_spikes, timestep):
    spike_ids = jnp.argwhere(neuron_spikes.astype(bool)).flatten().astype(float)
    neuron_spike_times = spike_ids * timestep if timestep != 1.0 else spike_ids 
    return neuron_spike_times


def convert_spike_times_to_raster(spike_times: np.ndarray, timestep: float = 1.0, max_time: Optional[float] = None, num_neurons: Optional[int] = None, dtype=None):
    """
    Convert spike times array to spike raster array. 
    For now, all neurons need to have same number of spike times.
    
    Args:
        spike_times: Array, spiketimes as array of shape batch_dim x spikes/neuron X 2
            training dim: (times, neuron_id)
    """

    if dtype is None:
        dtype = np.int16
    # spike_times = spike_times.astype(np.uint16)
    if num_neurons is None:
        num_neurons = int(np.nanmax(spike_times[:,:,1]))+1
    if max_time is None:
        max_time = np.nanmax(spike_times[:,:,0])
    num_bins = int(max_time / timestep + 1)
    
    spike_raster = np.zeros((spike_times.shape[0], num_bins, num_neurons), dtype=dtype)
    batch_id = np.arange(spike_times.shape[0]).repeat(spike_times.shape[1])
    spike_times_flat = (spike_times[:, :, 0].flatten() / timestep).astype(dtype)
    neuron_ids = spike_times[:, :, 1].flatten().astype(dtype)
    np.add.at(spike_raster, (batch_id, spike_times_flat, neuron_ids), 1)
    return spike_raster

