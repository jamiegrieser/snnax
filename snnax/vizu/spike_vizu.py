import matplotlib.pyplot as plt
import jax.numpy as jnp
from typing import Union, Sequence
from equinox.custom_types import Array

def visualize_spike_times_comparison(
        spike_times_1: Union[Array, Sequence[Union[Array, Sequence]]],
        spike_times_2: Union[Array, Sequence[Union[Array, Sequence]]],
        title=None,
    ):
    
    assert len(spike_times_1) == len(spike_times_2)

    line_length = 0.4
    num_neurons = len(spike_times_1)
    lineoffsets_1 = jnp.arange(num_neurons)-line_length/2
    lineoffsets_2 = jnp.arange(num_neurons)+line_length/2
    hline_ys = (lineoffsets_1[1:]+lineoffsets_2[:-1])/2

    plt.figure()                              
    plt.eventplot(spike_times_1, color="C0", linelengths=line_length, lineoffsets=lineoffsets_1)
    plt.eventplot(spike_times_2, color="C1", linelengths=line_length, lineoffsets=lineoffsets_2)

    xmin, xmax, ymin, ymax = plt.axis()
    plt.hlines(hline_ys, xmin, xmax, linestyles="dashed", color="black", linewidth=1, alpha=0.5)

    if title is not None:
        plt.title(title)
    plt.xlabel('Spike')
    plt.ylabel('Neuron')
