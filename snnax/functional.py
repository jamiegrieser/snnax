import jax
import jax.numpy as jnp
from jax import custom_jvp


# TODO set default to 1. or 10. as in 
# 'The remarkable robustness of surrogate gradient learning 
# for instilling complex function in spiking neural networks'
# by Zenke and Vogels (https://www.biorxiv.org/content/10.1101/2020.06.29.176925v1) 
def get_heaviside_with_super_spike_surrogate(beta=10.): # 

    @custom_jvp
    def heaviside_with_super_spike_surrogate(x):
        return jnp.heaviside(x, 1)

    @heaviside_with_super_spike_surrogate.defjvp
    def f_jvp(primals, tangents):
        x, = primals
        x_dot, = tangents
        primal_out = heaviside_with_super_spike_surrogate(x)
        tangent_out = 1/(beta*jnp.abs(x)+1) * x_dot
        return primal_out, tangent_out
    
    return heaviside_with_super_spike_surrogate


sigmoid = jax.nn.sigmoid

# TODO set default to 1. or 10. as in 
# 'The remarkable robustness of surrogate gradient learning 
# for instilling complex function in spiking neural networks'
# by Zenke and Vogels (https://www.biorxiv.org/content/10.1101/2020.06.29.176925v1) 
def get_heaviside_with_sigmoid_surrogate(beta=1.): # set default to 1. or 10. ?
    assert float(beta) == 1.0, "Currently only beta = 1.0 is supported for numerical stability."
    @custom_jvp
    def heaviside_with_sigmoid_surrogate(x):
        return jnp.heaviside(x, 1)

    @heaviside_with_sigmoid_surrogate.defjvp
    def f_jvp(primals, tangents):
        x, = primals
        x_dot, = tangents
        primal_out = heaviside_with_sigmoid_surrogate(x)
        # TODO multiplication by beta correct here?
        tangent_out = sigmoid(x, beta) * (1-beta*sigmoid(x, beta)) * x_dot 
        return primal_out, tangent_out

    return heaviside_with_sigmoid_surrogate


softplus = jax.nn.softplus


def inverse_softplus(x): #, beta: float = 1.0):
    return jnp.log(jnp.exp(x)-1) # / beta


# Dictionary containing available spike surrogate functions
SURROGATE_DICT = {
        "superspike": get_heaviside_with_super_spike_surrogate,
        "sigmoid": get_heaviside_with_sigmoid_surrogate,
    }


def get_spike_fn(surrogate : str, *args, **kwargs):
    """
    Function returns corresponding callable surrogate function
    based on the given string.
    """
    if surrogate not in SURROGATE_DICT:
        raise ValueError(f"Unkown surrogate function '{surrogate}'")
                            # f", got {','.join("'{surr}'" for surr in surr_dict)}")
    return SURROGATE_DICT[surrogate](*args, **kwargs)

